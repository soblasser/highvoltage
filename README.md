# Interactive High-voltage Experiments with Unity3D #

This repository contains the content of the "MyAssets" folder of the Unity3D project.
More detailed:

* Scenes
* 3D Models
* Prefabs
* Scripts
* Textures
* Sounds
* Materials

There are external assets needed to make the project run.

### External Unity packages being used ###

* Magnetodynamics
* Vectrosity
* ProceduralLightning

### Screenshot ###
![VdGExp1][1]

[1]: screenshots/screenshot_vdg1.png