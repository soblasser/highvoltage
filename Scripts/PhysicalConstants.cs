﻿public static class PhysicalConstants
{
	/// <summary>
	/// The permittivity of free space in [C^2/Nm^2]
	/// </summary>
	public const float e0 = 8.8541878176e-12f;
	/// <summary>
	/// The absolute charge of one electron in [C]
	/// </summary>
	public const float e = 1.6021766208e-19f;
}
