﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class WhiteboardController : MonoBehaviour 
{
	private List<Lecture> lectures;
	private Lecture selectedLecture;
	private int currentWebContentIndex;
	private Renderer myRenderer;
	private Object lockObject = new Object();
	private bool loaded;
	
	public List<Lecture> Lectures{ get{ return this.lectures; } }
	public Lecture SelectedLecture{ get{ return this.selectedLecture; } }
	public int CurrentWebContentIndex{ get{ return this.currentWebContentIndex; } }
	public bool Loaded { get{ return this.loaded; } }

	#if UNITY_WEBPLAYER || UNITY_WEBGL
	public IEnumerator Start () {
		this.myRenderer = this.GetComponent<Renderer> ();
		this.currentWebContentIndex = -1;
		string path = Path.Combine (Application.dataPath, "lectures.xml");

		if(Application.isEditor){
			this.LoadXmlFileFromLocalDrive(path);
		}
		else {
			// Load lectures XML file via URL
			var www = new WWW(path);
			yield return www;
			this.lectures = LectureContainer.LoadFromText(www.text).Lectures;
			this.loaded = true;
		}
	}
	#else
	public void Start () {
		this.myRenderer = this.GetComponent<Renderer> ();
		this.currentWebContentIndex = -1;
		// Load lectures XML file from local drive (e.g. for standalone build)
		this.LoadXmlFileFromLocalDrive(Path.Combine (Application.dataPath, "lectures.xml"));
	}
	#endif

	private void LoadXmlFileFromLocalDrive(string path)
	{
		this.lectures = LectureContainer.Load(path).Lectures;
		this.loaded = true;
	}
	
	public void Update () {
		if (null == this.lectures)
			return;
		foreach (Lecture lecture in this.lectures)
			lecture.StartReloadingFailedWebContents();
	}

	public void SelectLecture(int lectureIndex)
	{
		if (lectureIndex < 0 || lectureIndex >= this.lectures.Count) {
			throw new  System.IndexOutOfRangeException(string.Format("Selected Lecture Index {0} out of range", lectureIndex));
		}
		this.selectedLecture = this.lectures [lectureIndex];
		this.currentWebContentIndex = 0;
	}

	public void Refresh()
	{
		if (null == this.selectedLecture) {
			return;
		}
		if (null == this.selectedLecture.WebContents)
			throw new System.NullReferenceException("Web contents list in selected lecture is null");
		lock (this.lockObject) {
			if (this.currentWebContentIndex >= 0 && this.selectedLecture.WebContents.Count > this.currentWebContentIndex) {
				this.LoadTexture (this.selectedLecture.WebContents [this.currentWebContentIndex]);
			}
		}
	}

	public void Next() 
	{
		if (null == this.selectedLecture) {
			return;
		}
		if (null == this.selectedLecture.WebContents)
			throw new System.NullReferenceException("Web contents list in selected lecture is null");
		lock (this.lockObject) {
			if (this.selectedLecture.WebContents.Count > this.currentWebContentIndex + 1) {
				this.LoadTexture (this.selectedLecture.WebContents [++this.currentWebContentIndex]);
			}
		}
	}

	public void Previous() 
	{
		if (null == this.selectedLecture) {
			return;
		}
		if (null == this.selectedLecture.WebContents)
			throw new System.NullReferenceException("Web contents list in selected lecture is null");
		lock (this.lockObject) {
			if (this.currentWebContentIndex > 0) {
				this.LoadTexture (this.selectedLecture.WebContents [--this.currentWebContentIndex]);
			}
		}
	}

	private bool LoadTexture(WWW webContent)
	{
		// File name must be of type *.png, *.jpg or *.jpeg, otherwise a crossdomain.xml missing 
		// exception will be thrown when running as a webplayer and trying to access content from
		// an other domain.
		if (null == webContent || !webContent.isDone || null != webContent.error) {
			this.myRenderer.material.mainTexture = Texture2D.whiteTexture;
			return false;
		}
		this.myRenderer.material.mainTexture = webContent.texture;
		return true;
	}
}
