﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ColliderEntered : MonoBehaviour {

	public string LevelName;
	public string DisplayedText;	// starts with "Press [E] "

	private GameObject player;
	private bool insideTriggerSphere = false;
	private GUIStyle textStyle;

	public void Start()
	{
		this.textStyle = new GUIStyle("label");
		this.textStyle.alignment = TextAnchor.MiddleCenter;
		this.textStyle.normal.textColor = Color.white;

		// find Player
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		if (null == player) {
			throw new System.Exception ("GameObject with Player tag not found");
		}
		this.player = player;
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) {
			Debug.Log("Player entered");
			this.insideTriggerSphere = true;
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.CompareTag ("Player")) {
			Debug.Log("Player exit");
			this.insideTriggerSphere = false;
		}
	}

	public void Update()
	{
		if (Input.GetKeyDown (KeyCode.E) && this.insideTriggerSphere) {
			Debug.Log(LevelName);

			// Store current player position and rotation in static variables
			if (null != this.player) {
				Player.position = this.player.transform.position;
				Player.rotation = this.player.transform.rotation;
			}

			SceneManager.LoadScene (LevelName);
		}
	}

	public void OnGUI()
	{
		if (this.insideTriggerSphere) {
			GUI.Label(new Rect(Screen.width / 2 - 200f, Screen.height / 2 - 100f, 400f, 200f), "Press [E] " + DisplayedText, this.textStyle);
		}
	}
}
