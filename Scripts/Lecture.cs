﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

/// <summary>
/// Some dummy container to ease XML serialization.
/// </summary>
[XmlRoot("lectureCollection")]
public class LectureContainer
{
	private List<Lecture> lectures;

	public LectureContainer() {}

	[XmlArray("lectures"),XmlArrayItem("lecture")]
	public List<Lecture> Lectures{ get{ return this.lectures; } set{ this.lectures = value; }}

	public static LectureContainer Load(string path)
	{
		var serializer = new XmlSerializer(typeof(LectureContainer));

		using (var stream = new FileStream(path, FileMode.Open)) {
			LectureContainer container = serializer.Deserialize(stream) as LectureContainer;
			// Start loading web content
			container.StartLoadingAllWebContents();
			return container;
		}
	}

	public static LectureContainer LoadFromText(string text)
	{
		var serializer = new XmlSerializer(typeof(LectureContainer));
		LectureContainer container = serializer.Deserialize(new StringReader(text)) as LectureContainer;
		container.StartLoadingAllWebContents();
		return container;
	}

	private void StartLoadingAllWebContents() {
		foreach (Lecture lecture in this.lectures)
			lecture.StartLoadingWebContents ();
	}

	public void Save(string path)
	{
		var serializer = new XmlSerializer(typeof(LectureContainer));
		using (var stream = new FileStream(path, FileMode.Create)) {
			serializer.Serialize(stream, this);
		}
	}
}

[XmlRoot("lecture")]
public class Lecture 
{
	/// <summary>
	/// The name of the lecture.
	/// </summary>
	private string name;
	/// <summary>
	/// The URLs of the web content.
	/// </summary>
	private List<string> urls;
	/// <summary>
	/// The list of web content objects.
	/// </summary>
	private List<WWW> webContents;

	[XmlAttribute("name")]
	public string Name { get{ return this.name; } set{ this.name = value; }}

	[XmlArray("webContents"),XmlArrayItem("url")]
	public List<string> ContentUrls { get{ return this.urls; } set{ this.urls = value; }}

	[XmlIgnore]
	public List<WWW> WebContents { get{ return this.webContents; } }

	public Lecture(string name, List<string> urls) {
		this.name = name;
		this.urls = urls;
		this.StartLoadingWebContents ();
	}

	public Lecture() {
	}

	public bool IsDone()
	{
		foreach (WWW www in this.webContents) {
			if(null != www) {
				if(!www.isDone)
					return false;
			}
		}
		return true;
	}

	/// <summary>
	/// Asynchronous method that starts loading the web contents.
	/// </summary>
	public void StartLoadingWebContents()
	{
		if(null == this.urls)
			throw new System.NullReferenceException("Cannot start loading web contents because list of URLs is null");

		this.webContents = new List<WWW>();
		foreach (string url in this.urls) {
			this.webContents.Add(new WWW(url));
		}
	}

	/// <summary>
	/// Asynchronous method that starts reloading web contents of
	/// which downloads finished but contained an error.
	/// </summary>
	public void StartReloadingFailedWebContents()
	{
		if (null == this.webContents)
			return;
		
		int i = 0;
		foreach (WWW webContent in this.webContents) 
		{
			if(null != webContent && webContent.isDone && null != webContent.error)
			{
				this.webContents[i] = new WWW(webContent.url);
			}
			i++;
		}
	}
}
