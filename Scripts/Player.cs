﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	// Initial values for position and rotation of the player's transform
	public static Vector3 position = new Vector3(0.2f, 0.98f, 4.21f);
	public static Quaternion rotation = new Quaternion(0f, 180f, 0f, 0f);

	/// <summary>
	/// Load the player's transform position and rotation at startup.
	/// </summary>
	public void Start () {
		this.transform.position = position;
		this.transform.rotation = rotation;
	}
}
