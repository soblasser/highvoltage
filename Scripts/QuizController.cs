﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class QuizController : MonoBehaviour 
{
	private List<Quiz> quizzes;
	private Quiz selectedQuiz;
	private int currentQuestionIndex;
	private Object lockObject = new Object();
	private bool loaded;

	public List<Quiz> Quizzes { get{ return this.quizzes; }}
	public Quiz SelectedQuiz { get{ return this.selectedQuiz; } }
	public int CurrentQuestionIndex { get{ return this.currentQuestionIndex; } }
	public bool Loaded { get{ return this.loaded; } }

#if UNITY_WEBPLAYER || UNITY_WEBGL
	public IEnumerator Start () {
		this.currentQuestionIndex = -1;
		string path = Path.Combine (Application.dataPath, "quizzes.xml");

		if(Application.isEditor){
			this.LoadQuizzesFromLocalDrive(path);
		}
		else {
			// Load quizzes from XML via URL
			var www = new WWW(path);
			yield return www;
			this.quizzes = QuizContainer.LoadFromText (www.text).Quizzes;
			this.loaded = true;
		}
	}
#else
	public void Start () {
		this.currentQuestionIndex = -1;
		// Load quizzes from XML on local drive (e.g. for standalone build)
		this.LoadQuizzesFromLocalDrive(Path.Combine (Application.dataPath, "quizzes.xml"));
	}
#endif

	private void LoadQuizzesFromLocalDrive(string path)
	{
		this.quizzes = QuizContainer.Load(path).Quizzes;
		this.loaded = true;
	}

	public void Update () {
	
	}

	public string[] GetQuizNames()
	{
		string[] quizNames = new string[this.quizzes.Count];
		int i = 0;
		foreach (Quiz quiz in this.quizzes) {
			quizNames[i++] = string.Format("{0} / Score: {1:0.00}%", quiz.Name, quiz.GetScore());
		}
		return quizNames;
	}

	public Quiz SelectQuiz(int quizIndex)
	{
		if (quizIndex < 0 || quizIndex >= this.quizzes.Count) {
			throw new  System.IndexOutOfRangeException(string.Format("Selected Quiz Index {0} out of range", quizIndex));
		}
		this.selectedQuiz = this.quizzes [quizIndex];
		this.currentQuestionIndex = 0;
		return this.selectedQuiz;
	}

	public Question GetCurrentQuestion()
	{
		if (null == this.selectedQuiz.Questions)
			throw new System.NullReferenceException("Questions list in selected quizz is null");
		lock (this.lockObject) {
			if (this.currentQuestionIndex >= 0 && this.selectedQuiz.Questions.Count > this.currentQuestionIndex) {
				return this.selectedQuiz.Questions[this.currentQuestionIndex];
			}
		}
		return null;
	}

	public Question GetNextQuestion()
	{
		if (null == this.selectedQuiz.Questions)
			throw new System.NullReferenceException("Questions list in selected quizz is null");
		lock (this.lockObject) {
			if (this.currentQuestionIndex >= 0 && this.selectedQuiz.Questions.Count > this.currentQuestionIndex + 1) {
				return this.selectedQuiz.Questions[++this.currentQuestionIndex];
			}
		}
		return null;
	}

	public bool AnswerCurrentQuestion(int answerIndex)
	{
		if (null == this.selectedQuiz.Questions)
			throw new System.NullReferenceException("Questions list in selected quizz is null");
		
		return this.selectedQuiz.Questions[this.currentQuestionIndex].Answer(answerIndex);
	}
}
