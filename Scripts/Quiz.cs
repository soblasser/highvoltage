﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

/// <summary>
/// Some dummy container to ease XML serialization.
/// </summary>
[XmlRoot("quizCollection")]
public class QuizContainer
{
	private List<Quiz> quizzes;

	public QuizContainer() { }

	[XmlArray("quizzes"),XmlArrayItem("quiz")]
	public List<Quiz> Quizzes { get{ return this.quizzes; } set{ this.quizzes = value; } }

	public static QuizContainer Load(string path)
	{
		var serializer = new XmlSerializer(typeof(QuizContainer));
		
		using (var stream = new FileStream(path, FileMode.Open)) {
			return serializer.Deserialize(stream) as QuizContainer;
		}
	}

	public static QuizContainer LoadFromText(string text)
	{
		var serializer = new XmlSerializer(typeof(QuizContainer));
		return serializer.Deserialize(new StringReader(text)) as QuizContainer;
	}
	
	public void Save(string path)
	{
		var serializer = new XmlSerializer(typeof(QuizContainer));
		using (var stream = new FileStream(path, FileMode.Create)) {
			serializer.Serialize(stream, this);
		}
	}
}

[XmlRoot("quiz")]
public class Quiz 
{
	private string name;
	private List<Question> questions;

	[XmlAttribute("name")]
	public string Name { get{ return this.name; } set{ this.name = value; } }

	[XmlArray("questions"),XmlArrayItem("question")]
	public List<Question> Questions { get{ return this.questions; } set{ this.questions = value; } }
	
	public Quiz(string name, List<Question> questions)
	{
		this.name = name;
		this.questions = questions;
	}

	public Quiz() { }

	public void Reset()
	{
		foreach (Question question in this.questions) {
			question.ResetAnswer();
		}
	}

	/// <summary>
	/// Get the percentage of correctly answered questions.
	/// </summary>
	/// <returns>The score in percent</returns>
	public float GetScore()
	{
		int sumOfCorrectlyAnsweredQuestions = 0;
		foreach(Question question in this.questions)
		{
			if (question.AnsweredCorrectly)
				sumOfCorrectlyAnsweredQuestions++;
		}
		return 100.0f * sumOfCorrectlyAnsweredQuestions / this.questions.Count;
	}
}
