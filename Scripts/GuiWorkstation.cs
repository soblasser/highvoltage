﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GuiWorkstation : MonoBehaviour 
{
	private QuizController quizController;
	private bool showQuizSelectionMenu = true;
	private Vector2 scrollViewVector = Vector2.zero;
	private int selectedQuizIndex = -1;
	private int selectedAnswerIndex = -1;
	private string[] quizNames;
	private GUIStyle textStyleQuestionText;
	private GUIStyle textStyleAnswerCorrect;
	private GUIStyle textStyleAnswerWrong;
	private bool loaded;
	
	public void Start () {
		// Define GUI styles
		this.textStyleQuestionText = new GUIStyle("label");
		this.textStyleQuestionText.alignment = TextAnchor.MiddleCenter;

		this.textStyleAnswerCorrect = new GUIStyle("label");
		this.textStyleAnswerCorrect.alignment = TextAnchor.MiddleCenter;
		this.textStyleAnswerCorrect.normal.textColor = Color.green;

		this.textStyleAnswerWrong = new GUIStyle("label");
		this.textStyleAnswerWrong.alignment = TextAnchor.MiddleCenter;
		this.textStyleAnswerWrong.normal.textColor = Color.red;

		// Find Workstation GameObject in the scene
		GameObject workstation = GameObject.FindGameObjectWithTag ("Workstation");
		if (null == workstation) {
			throw new System.NullReferenceException("No Workstation GameObject found");
		}
		this.quizController = workstation.GetComponent<QuizController> ();
		if (null == this.quizController) {
			throw new System.NullReferenceException("No QuizController script attached to Workstation GameObject");
		}
	}

	public void Update () 
	{
		if (!this.loaded && null != this.quizController && this.quizController.Loaded) {
			// Get names of quizzes
			this.quizNames = this.quizController.GetQuizNames();
			this.loaded = true;
		}

		// Check if [ESC] was pressed
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			SceneManager.LoadScene("Laboratory");
		}
	}

	public void OnGUI()
	{
		// Show control messages on top left corner
		GUI.Label (new Rect (10f, 10f, 300f, 200f), string.Format("[ESC] - Leave"));

		// Show quiz selection menu when activated
		if (this.showQuizSelectionMenu) {
			this.ShowQuizSelectionMenu();
		} 
		else {
			// Show selected quiz question
			this.ShowSelectedQuizQuestion (this.quizController.GetCurrentQuestion());
		}

		if (GUI.changed) {
			if(this.showQuizSelectionMenu) {
				Debug.Log("Selected Quiz Index: " + this.selectedQuizIndex);
				if(null != this.quizController) {
					this.showQuizSelectionMenu = false;
					this.quizController.SelectQuiz(this.selectedQuizIndex);
					this.quizController.SelectedQuiz.Reset();
					this.quizController.GetCurrentQuestion();
				}
			}
			else if(this.selectedAnswerIndex != -1){
				// Handle question answer of selected quiz
				Debug.Log ("Selected Answer Index: " + this.selectedAnswerIndex);
				if (null != this.quizController) {
					// answer the question
					this.quizController.AnswerCurrentQuestion(this.selectedAnswerIndex);
					// wait and get the next question
					StartCoroutine(this.WaitAndGetNextQuestion());
				}
				this.selectedAnswerIndex = -1;
			}
		}
	}

	private void ShowQuizSelectionMenu()
	{
		if (null == this.quizNames)
			return;

		// Begin the ScrollView
		this.scrollViewVector = GUI.BeginScrollView (new Rect (Screen.width / 2f - 225f, 50f, 450f, Screen.height - 280f), this.scrollViewVector, new Rect (0f, 0f, 430f, 2000f));
		
		// Put something inside the ScrollView
		GUILayout.BeginArea (new Rect (0, 0, 430f, 2000f));
		GUILayout.Box ("PLEASE SELECT A QUIZ");
		this.selectedQuizIndex = GUILayout.SelectionGrid (this.selectedQuizIndex, this.quizNames, 1);
		GUILayout.EndArea ();
		
		// End the ScrollView
		GUI.EndScrollView ();
	}

	private void ShowSelectedQuizQuestion(Question question)
	{
		if (null == question)
			return;

		GUILayout.BeginArea (new Rect (Screen.width / 2f - 350f, 200f, 700f, Screen.height - 280f));
		GUILayout.Label (string.Format ("Question {0}/{1}:", this.quizController.CurrentQuestionIndex + 1, this.quizController.SelectedQuiz.Questions.Count));
		GUILayout.TextArea (question.Text, this.textStyleQuestionText);
		GUILayout.Space (10);
		int selectedIndex = GUILayout.SelectionGrid (this.selectedAnswerIndex, question.GetAnswerTexts (), 2);
		// only accept the selection when the answer has not been given yet
		if(!question.Answered){
			this.selectedAnswerIndex = selectedIndex;
		}
		else {
			// show if the given answer is correct or wrong
			GUILayout.Space (20);
			GUILayout.Label (question.AnsweredCorrectly ? "Well done, this is correct!" : "Unfortunately this is wrong, please go to the experiment to figure out the correct answer!", question.AnsweredCorrectly ? this.textStyleAnswerCorrect : this.textStyleAnswerWrong);
		}

		GUILayout.EndArea ();
	}

	private IEnumerator WaitAndGetNextQuestion()
	{
		yield return new WaitForSeconds(2);
		this.showQuizSelectionMenu = (null == this.quizController.GetNextQuestion ());
		if (this.showQuizSelectionMenu) {
			// if it was the last question of the quiz, reload the quiz names with updated score
			this.quizNames = this.quizController.GetQuizNames ();
		}
	}
}
