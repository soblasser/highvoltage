﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("answer")]
public class Answer
{
	private string text;
	private bool correct;
	
	[XmlText]
	public string Text { get{ return this.text; } set{ this.text = value; } }

	[XmlAttribute("correct")]
	public bool Correct { get{ return this.correct; } set{ this.correct = value; } }

	public Answer(string text, bool correct)
	{
		this.text = text;
		this.correct = correct;
	}

	public Answer() { }

	public override string ToString ()
	{
		return this.text;
	}
}

[XmlRoot("question")]
public class Question 
{
	private string text;
	private List<Answer> answers;
	private bool answered;
	private bool answeredCorrectly;

	[XmlElement("text")]
	public string Text { get{ return this.text; } set{ this.text = value; } }

	[XmlArray("answers"),XmlArrayItem("answer")]
	public List<Answer> Answers { get{ return this.answers; } set{ this.answers = value; } }

	[XmlIgnore]
	public bool Answered { get{ return this.answered; } }
	[XmlIgnore]
	public bool AnsweredCorrectly { get{ return this.answeredCorrectly; } }

	public Question(string text, List<Answer> answers)
	{
		this.text = text;
		this.answers = answers;
	}

	public Question() { }

	public string[] GetAnswerTexts()
	{
		if(null == this.answers)
			throw new System.ArgumentNullException("List of answers for this question is null");
		
		string[] answerTexts = new string[this.answers.Count];
		int i = 0;
		foreach (Answer answer in this.answers) {
			answerTexts[i++] = answer.ToString();
		}
		return answerTexts;
	}

	public bool Answer(int answerIndex)
	{
		if(null == this.answers)
			throw new System.ArgumentNullException("List of answers for this question is null");

		if (answerIndex < 0 || answerIndex >= this.answers.Count)
			throw new System.ArgumentOutOfRangeException (string.Format("Answer Index {0} is out of range for this set of answers {1}", answerIndex, this.answers.Count));
			
		this.answered = true;
		this.answeredCorrectly = this.answers[answerIndex].Correct;
		return this.answeredCorrectly;
	}

	public void ResetAnswer()
	{
		this.answered = false;
		this.answeredCorrectly = false;
	}
}
